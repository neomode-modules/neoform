Pod::Spec.new do |s|

  s.name         = "Neoform"
  s.version      = "1.3.0"
  s.summary      = "A form validation framework that does not interfere with the form UI."
  s.description  = "A form validation framework that allows you to create the form interface anyway you want while providing masks and value validation on code."

  s.homepage     = "http://www.neomode.com.br"
  s.license      = { :type => "Apache License, Version 2.0", :file => "LICENSE" }
  s.author    = "Neomode"
  s.platform     = :ios, "10.0"
  s.source       = { :git => "https://gitlab.com/neomode-modules/neoform", :tag => s.version.to_s }

  s.source_files  = "Neoform", "Neoform/**/*.{h,m,swift}"
  s.exclude_files = "Classes/Exclude"
  s.resources = ["Neoform/**/*.{storyboard,xib,xcassets}"]
  s.swift_version = '5.0'

end
