//
//  NeoformTextFieldFormats.swift
//  Neoform
//
//  Created by Edison Santiago on 01/04/17.
//  Copyright © 2017 Neomode Development Studio. All rights reserved.
//

import UIKit

public extension NeoformTextField {
    struct Formats {
        public struct optional: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 0
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.default
        }
        
        public struct nonEmpty: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 1
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.default
        }
        
        public struct email: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 1
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.emailAddress
            
            public func getValidValue(from value: String) throws -> String {
                if self.minCharactersAllowed > 0 && value.count == 0 {
                    throw NeoformError.empty(element: nil)
                }
                
                if !(value.count >= self.minCharactersAllowed && value.count <= self.maxCharactersAllowed) {
                    throw NeoformError.invalidValue(textField: nil)
                }
                
                //Check for a valid email on the field
                let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
                let matches = detector.matches(in: value, options: [], range: NSMakeRange(0, value.count))
                
                var email: String? = nil
                for match in matches {
                    if let matchURL = match.url,
                        let matchURLComponents = URLComponents(url: matchURL, resolvingAgainstBaseURL: false),
                        matchURLComponents.scheme == "mailto"
                    {
                        email = matchURLComponents.path
                    }
                }
                
                guard let validEmail = email else {
                    throw NeoformError.invalidValue(textField: nil)
                }
                
                return validEmail.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
            }
        }
        
        public struct optionalNumbersOnly: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 0
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
        }
        
        public struct numbersOnly: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 1
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
        }
        
        public struct pin4: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 4
            public var maxCharactersAllowed: Int = 4
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
        }
        
        public struct pin6: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 6
            public var maxCharactersAllowed: Int = 6
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
        }
        
        public struct lettersOnly: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 1
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.letters.union(CharacterSet.whitespaces)
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.letters.union(CharacterSet.whitespaces)
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.default
        }
        
        public struct currency: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 1
            public var maxCharactersAllowed: Int = 20
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
                .union(CharacterSet(charactersIn: Locale.current.currencyCode ?? ""))
                .union(CharacterSet(charactersIn: Locale.current.currencySymbol ?? ""))
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
            
            lazy var currencyFormatter: NumberFormatter = {
                let fmt = NumberFormatter()
                fmt.locale = Locale.current
                fmt.numberStyle = .currency
                fmt.minimumFractionDigits = 2
                fmt.maximumFractionDigits = 2
                return fmt
            }()
            
            public mutating func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
                //First check for invalid characters on the replacementString given
                if string.rangeOfCharacter(from: self.allowedTypedCharactersSet.inverted) != nil {
                    return false
                }
                
                let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
                
                //Check if the new string will exceed the maximum of characters allowed
                if newString.count > self.maxCharactersAllowed {
                    return false
                }
                
                //Do not allow to the delete the currencyCode
                if string.count == 0, newString.count < self.currencyFormatter.currencySymbol.count {
                    return false
                }
                
                //Try to format the newString
                if let formattedString = self.format(newString) {
                    textField.text = formattedString
                }
                
                return false
            }
            
            public mutating func format(_ string: String) -> String? {
                var newNumber: Double!
                
                //Remove the dots and commas from the number to allow the formatting
                let stringToFormat = string.replacingOccurrences(of: self.currencyFormatter.currencyDecimalSeparator, with: "").replacingOccurrences(of: self.currencyFormatter.currencyGroupingSeparator, with: "")
                
                if let valueNumber = self.currencyFormatter.number(from: stringToFormat) {
                    //Got number, so format it.
                    newNumber = valueNumber.doubleValue / 100.0
                }
                else {
                    //First time typing on the field, so create a number from the string
                    newNumber = Double(stringToFormat)
                }
                
                return self.currencyFormatter.string(from: NSNumber(value: newNumber))
            }
        }
        
        public struct cpf: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 14
            public var maxCharactersAllowed: Int = 14
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: ".-"))
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
            
            public func format(_ string: String) -> String? {
                let onlyNumbersStr = string.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
                
                if onlyNumbersStr.count < 3 {
                    return onlyNumbersStr
                }
                
                var formattedCpf = ""
                for i in 0..<4 {
                    if onlyNumbersStr.count < i*3 {
                        continue
                    }
                    
                    var piece = onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: i*3)...]
                    let indexToAdd = piece.count > 3 ? 3 : piece.count
                    if indexToAdd == 0 {
                        continue
                    }
                    piece = piece[..<piece.index(piece.startIndex, offsetBy: indexToAdd)]
                    if i == 1 || i == 2 {
                        formattedCpf += "."
                    }
                    if i == 3 {
                        formattedCpf += "-"
                    }
                    formattedCpf += piece
                }
                
                return formattedCpf
            }
            
            public func getValidValue(from value: String) throws -> String {
                if self.minCharactersAllowed > 0 && value.count == 0 {
                    throw NeoformError.empty(element: nil)
                }
                
                if !(value.count >= self.minCharactersAllowed && value.count <= self.maxCharactersAllowed) {
                    throw NeoformError.invalidValue(textField: nil)
                }
                
                //We need to validate the cpf digit to check if it's really valid
                //First of all get only the numbers
                let onlyNumbersStr = value.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
                
                /*
                var firstDigitSum: Int = 0
                for i in 0..<9 {
                    let numberStr = onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: i)]
                    
                    firstDigitSum = firstDigitSum + (Int(String(numberStr))! * (10-i))
                }
                
                let firstDigitValue = (firstDigitSum % 11 < 2) ? 0 : (11 - firstDigitSum % 11)
                
                let currentFirstDigit = Int(String(onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 9)]))
                
                guard currentFirstDigit == firstDigitValue else {
                    throw NeoformError.invalidValue(textField: nil)
                }
                
                var secondDigitSum: Int = 0
                for i in 0..<10 {
                    let numberStr = onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: i)]
                    
                    secondDigitSum = secondDigitSum + (Int(String(numberStr))! * (11-i))
                }
                
                let secondDigitValue = (secondDigitSum % 11 < 2) ? 0 : (11 - secondDigitSum % 11)
                
                let currentSecondDigit = Int(String(onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 10)]))
                
                guard currentSecondDigit == secondDigitValue else {
                    throw NeoformError.invalidValue(textField: nil)
                }*/
                
                guard self.checkCpfDigits(forCpf: onlyNumbersStr) else {
                    throw NeoformError.invalidValue(textField: nil)
                }
                
                return value.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
            }
            
            fileprivate func checkCpfDigits(forCpf cpf: String) -> Bool {
                return self.checkCpfDigitValid(forCpf: cpf, firstDigit: true) && self.checkCpfDigitValid(forCpf: cpf, firstDigit: false)
            }
            
            fileprivate func checkCpfDigitValid(forCpf cpf: String, firstDigit: Bool) -> Bool {
                let digitPosition = firstDigit ? 10 : 11
                
                var digitSum: Int = 0
                for i in 0..<digitPosition - 1 {
                    let numberStr = cpf[cpf.index(cpf.startIndex, offsetBy: i)]
                    
                    digitSum = digitSum + (Int(String(numberStr))! * (digitPosition-i))
                }
                
                let digitValue = (digitSum % 11 < 2) ? 0 : (11 - digitSum % 11)
                
                let currentDigitValue = Int(String(cpf[cpf.index(cpf.startIndex, offsetBy: digitPosition - 1)]))!
                
                return digitValue == currentDigitValue
            }
        }
        
        public struct cep: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 9
            public var maxCharactersAllowed: Int = 9
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits.union(CharacterSet(charactersIn: "-"))
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
            
            public func format(_ string: String) -> String? {
                let onlyNumbersStr = string.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
                
                //No format to do
                if onlyNumbersStr.count < 6 {
                    return onlyNumbersStr
                }
                
                let firstPart = onlyNumbersStr[..<onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 5)]
//                let firstPart = onlyNumbersStr.substring(to: onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 5))
                let lastPart = onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 5)...]
//                let lastPart = onlyNumbersStr.substring(from: onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 5))
                
                return "\(firstPart)-\(lastPart)"
            }
        }
        
        public struct phoneBr: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 14
            public var maxCharactersAllowed: Int = 15
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits.union(CharacterSet.whitespaces).union(CharacterSet(charactersIn: "()-"))
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
            
            public func format(_ string: String) -> String? {
                let areaCodeSize = 2
                var firstPartSize = 4
                let lastPartSize = 4
                
                let onlyNumbersStr = string.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
                
                //Empty string
                if onlyNumbersStr.count == 0 {
                    return ""
                }
                
                //Not the entire area code
                if onlyNumbersStr.count < areaCodeSize {
                    return "(\(onlyNumbersStr)"
                }
                
                //SET the formatted area code
                var formattedPhone = "("
                let areaCode = onlyNumbersStr[..<onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 2)]
//                let areaCode = onlyNumbersStr.substring(to: onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: 2))
                formattedPhone += "\(areaCode)"
                
                
                
                //Check if have the firstPart
                if onlyNumbersStr.count <= areaCodeSize {
                    return "\(formattedPhone)"
                }
                
                //Check if the first part should have 4 or 5 numbers
                if onlyNumbersStr.count > 10 {
                    firstPartSize = 5
                }
                
                //Check if we have the entire first part
                var firstPartEndIndexCount = areaCodeSize + firstPartSize
                if onlyNumbersStr.count < firstPartEndIndexCount {
                    firstPartEndIndexCount = onlyNumbersStr.count
                }
                
                //Create index for the first part
                let firstPartStartIndex = onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: areaCodeSize)
                let firstPartEndIndex = onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: firstPartEndIndexCount)
//                let firstPartRange = firstPartStartIndex..<firstPartEndIndex
                
                //Create firstPart
                let firstPart = onlyNumbersStr[firstPartStartIndex..<firstPartEndIndex]
//                let firstPart = onlyNumbersStr.substring(with: firstPartRange)
                
                //Add to string
                formattedPhone += ") \(firstPart)"
                
                
                
                //Check if we have the secondPart
                if onlyNumbersStr.count <= areaCodeSize + firstPartSize {
                    return formattedPhone
                }
                
                //Check if we have the entire last part
                var lastPartEndIndexCount = areaCodeSize + firstPartSize + lastPartSize
                if onlyNumbersStr.count < lastPartEndIndexCount {
                    lastPartEndIndexCount = onlyNumbersStr.count
                }
                
                //Create index for the last part
                let lastPartStartIndex = onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: areaCodeSize + firstPartSize)
                let lastPartEndIndex = onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: lastPartEndIndexCount)
//                let lastPartRange = lastPartStartIndex..<lastPartEndIndex
                
                //Create lastPart
                let lastPart = onlyNumbersStr[lastPartStartIndex..<lastPartEndIndex]
//                let lastPart = onlyNumbersStr.substring(with: lastPartRange)
                
                //Add to string
                formattedPhone += "-\(lastPart)"
                
                return formattedPhone
            }
        }
        
        public struct creditCard: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 17
            public var maxCharactersAllowed: Int = 19
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits.union(CharacterSet.whitespaces)
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
            
            public func format(_ string: String) -> String? {
                let onlyNumbersStr = string.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
                
                if onlyNumbersStr.count < 4 {
                    return onlyNumbersStr
                }
                
                var formattedCard: String = ""
                
                for i in 0..<4 {
                    if onlyNumbersStr.count < i*4 {
                        continue
                    }
                    
                    var piece = onlyNumbersStr[onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: i*4)...]
//                    var piece = onlyNumbersStr.substring(from: onlyNumbersStr.index(onlyNumbersStr.startIndex, offsetBy: i*4))
                    let indexToAdd = piece.count > 4 ? 4 : piece.count
                    if indexToAdd == 0 {
                        continue
                    }
                    piece = piece[..<piece.index(piece.startIndex, offsetBy: indexToAdd)]
//                    piece = piece.substring(to: piece.index(piece.startIndex, offsetBy: indexToAdd))
                    if i != 0 {
                        formattedCard += " "
                    }
                    formattedCard += piece
                }
                
                return formattedCard
            }
        }
        
        public struct cvv: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 3
            public var maxCharactersAllowed: Int = 3
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
        }
        
        public struct password: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 6
            public var maxCharactersAllowed: Int = Int.max
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.controlCharacters.inverted
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.default
        }
        
        public struct oneTimeCode: NeoformTextFieldFormat {
            public var minCharactersAllowed: Int = 6
            public var maxCharactersAllowed: Int = 6
            
            public var allowedTypedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            public var allowedDisplayedCharactersSet: CharacterSet = CharacterSet.decimalDigits
            
            public var keyboardType: UIKeyboardType = UIKeyboardType.numberPad
        }
    }
    
    /*
     This index was created to allow us to set the field format straight from storyboard
    */
    enum FormatsIndex: String {
        case optional
        case nonEmpty
        case email
        case optionalNumbersOnly
        case numbersOnly
        case pin4
        case pin6
        case lettersOnly
        case currency
        case cpf
        case cep
        case phoneBr
        case creditCard
        case cvv
        case password
        case oneTimeCode
        
        var format: NeoformTextFieldFormat {
            switch self {
            case .optional:
                return NeoformTextField.Formats.optional()
            case .nonEmpty:
                return NeoformTextField.Formats.nonEmpty()
            case .email:
                return NeoformTextField.Formats.email()
            case .optionalNumbersOnly:
                return NeoformTextField.Formats.optionalNumbersOnly()
            case .numbersOnly:
                return NeoformTextField.Formats.numbersOnly()
            case .pin4:
                return NeoformTextField.Formats.pin4()
            case .pin6:
                return NeoformTextField.Formats.pin6()
            case .lettersOnly:
                return NeoformTextField.Formats.lettersOnly()
            case .currency:
                return NeoformTextField.Formats.currency()
            case .cpf:
                return NeoformTextField.Formats.cpf()
            case .cep:
                return NeoformTextField.Formats.cep()
            case .phoneBr:
                return NeoformTextField.Formats.phoneBr()
            case .creditCard:
                return NeoformTextField.Formats.creditCard()
            case .cvv:
                return NeoformTextField.Formats.cvv()
            case .password:
                return NeoformTextField.Formats.password()
            case .oneTimeCode:
                return NeoformTextField.Formats.oneTimeCode()
            }
        }
    }
}
