//
//  NeoformCheckElement.swift
//  Neoform
//
//  Created by Edison Santiago on 26/04/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import UIKit

/*
 The CheckElement is a view that
 
 */
@IBDesignable
public class NeoformCheckElement: UIView, NeoformElement {
    /*
     Conformance to NeoformElement protocol
     
     UITextField already conforms to canBecomeFirstResponder and becomeFirstResponder
     */
    @IBInspectable public var name: String!
    public var collection: NeoformElementCollection?
    public var _nextElement: NeoformElement?
    @IBInspectable public var errorMessage: String = ""
    
    /*
     Set a initial selected state to the check element from the storyboard
     
    */
    @IBInspectable public var startSelected: Bool = false
    
    //The flag that controls everything
    var selected: Bool = false
    
    //The icon and label outlets can be already set when we use the left/right defaults
    //Or they can be set manually if the user is using a custom view
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    //The contentView holds the view loaded from the xib file
    var contentView: UIView?
    
    @IBInspectable public var defaultMode: String?
    
    @IBInspectable public var selectedIcon: UIImage?
    @IBInspectable public var selectedIconTintColor: UIColor = UIColor.black
    
    @IBInspectable public var unselectedIcon: UIImage?
    @IBInspectable public var unselectedIconTintColor: UIColor = UIColor.black
    
    @IBOutlet weak var iconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var iconHeightConstraint: NSLayoutConstraint!
    
    @IBInspectable public var iconSize: CGSize = CGSize(width: 20, height: 20)
    
    @IBInspectable public var labelLocalizedText: String = ""
    @IBInspectable public var labelTextColor: UIColor = UIColor.black
    @IBInspectable public var labelFontSize: CGFloat = 14
    
    override public func awakeFromNib() {
        if self.subviews.count == 0 {
            //Using default mode!
            self.initDefaultView()
            
            //Set default icon
            self.selectedIcon = UIImage(named: "checked_default", in: Neoform.assetsBundle, compatibleWith: nil)!
            self.unselectedIcon = UIImage(named: "unchecked_default", in: Neoform.assetsBundle, compatibleWith: nil)!
            
            self.setDefaultModeValues()
        }
        else {
            //Using custom mode!
            //Check for a imageview and a label on the subviews
            for subview in self.subviews {
                if subview is UIImageView {
                    self.icon = (subview as! UIImageView)
                }
                if subview is UILabel {
                    self.label = (subview as! UILabel)
                }
            }
        }
        
        //Make sure that icon and label are not nil
        if self.icon == nil || self.label == nil {
            fatalError("A Custom NeoformCheckElement should have a UIImageView and a UILabel as subviews, which would be the icon and the label of the element.")
        }
        
        //Set as selected by default if specified by the storyboard. Otherwise, set as unselected.
        if self.startSelected {
            self.setSelected()
        }
        else {
            self.setUnselected()
        }
    }
    
    public override func prepareForInterfaceBuilder() {
        if self.subviews.count == 0 {
            self.initDefaultView()
            
            if self.startSelected {
                if let selectedImage = self.selectedIcon {
                    self.icon.image = selectedImage
                    self.icon.tintColor = self.selectedIconTintColor
                }
                else {
                    self.icon.backgroundColor = UIColor.black
                }
            }
            else {
                if let unselectedImage = self.unselectedIcon {
                    self.icon.image = unselectedImage
                    self.icon.tintColor = self.unselectedIconTintColor
                }
                else {
                    self.icon.layer.borderColor = UIColor.black.cgColor
                    self.icon.layer.borderWidth = 2.0
                }
            }
            
            self.setDefaultModeValues()
        }
    }
    
    func initDefaultView(){
        let defaultMode: NeoformCheckElement.DefaultViewMode = NeoformCheckElement.DefaultViewMode.init(rawValue: self.defaultMode ?? "") ?? .CheckLeft
        
        self.contentView = self.loadViewFromNib(forMode: defaultMode)
        self.contentView!.frame = self.bounds
        self.contentView!.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        self.contentView!.isUserInteractionEnabled = false
        
        self.addSubview(self.contentView!)
        self.sendSubviewToBack(self.contentView!)
    }
    
    func setDefaultModeValues() {
        //Set icon size
        self.iconWidthConstraint.constant = self.iconSize.width
        self.iconHeightConstraint.constant = self.iconSize.height
        
        //Set label data
        self.label.text = NSLocalizedString(self.labelLocalizedText, comment: "")
        self.label.textColor = self.labelTextColor
        self.label.font = self.label.font.withSize(self.labelFontSize)
    }
    
    func loadViewFromNib(forMode mode: NeoformCheckElement.DefaultViewMode) -> UIView {
        let nib = UINib(nibName: String(describing: type(of: self)) + mode.rawValue, bundle: Neoform.assetsBundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.customInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }
    
    func customInit() {
        //Create tap gesture
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(NeoformCheckElement.onViewTapped)))
        self.isUserInteractionEnabled = true
    }
    
    //Handle tap according to selection
    @objc func onViewTapped() {
        if self.selected {
            self.setUnselected()
        }
        else {
            self.setSelected()
        }
    }

    func setSelected() {
        if let checkCollection = self.collection as? NeoformCheckElementCollection {
            checkCollection.willSelectElement(self)
        }
        
        //Set the element as selected
        self.icon?.image = self.selectedIcon
        self.icon?.tintColor = self.selectedIconTintColor
        
        self.selected = true
    }
    
    //The force attribute is used on the unselectAll method of the collection
    //The collection needs to unselect everything when we are on the radio mode
    //So, the collection will only force a unselection when we already selected other element
    func setUnselected(force: Bool = false) {
        if !force, let checkCollection = self.collection as? NeoformCheckElementCollection {
            if !checkCollection.canUnselectElement() {
                return
            }
        }
        
        //Set the element as unselected
        self.icon?.image = self.unselectedIcon
        self.icon?.tintColor = self.unselectedIconTintColor
        
        self.selected = false
    }
}



extension NeoformCheckElement {
    public func validate() -> [String:Any] {
        return [self.name: self.selected]
    }
    
    public func handleError(error: NeoformError) throws {
        throw error
    }
}

extension NeoformCheckElement {
    enum DefaultViewMode: String {
        case CheckLeft
        case CheckRight
    }
}
