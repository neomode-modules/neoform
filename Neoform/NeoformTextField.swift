//
//  NeoformTextField.swift
//  Neoform
//
//  Created by Edison Santiago on 01/04/17.
//  Copyright © 2017 Neomode Development Studio. All rights reserved.
//

import UIKit

public class NeoformTextField: UITextField, NeoformElement {
    //TODO: Make another solution, more general than this one, that is specific to this project
    @IBInspectable public var errorButtonIconImage: UIImage = UIImage(named: "ic_info", in: Neoform.assetsBundle, compatibleWith: nil)!
    
    @IBInspectable public var errorButtonIconTintColor: UIColor = UIColor.red
    
    @IBInspectable public var errorButtonIconSize: CGSize = CGSize(width: 25, height: 25)
    
    @IBInspectable public var errorMessage: String = "The field is invalid."
    
    var showErrorButton: Bool {
        get {
            if let collection = self.collection {
                return collection.showErrorButton
            }
            else {
                return false
            }
        }
    }
    /*
    @IBInspectable public var fieldErrorValidationMessage: String {
        get {
            return self.errorMessage
        }
        set {
            self.errorMessage = newValue
        }
    }
    */
    
    /*
     By default the error button will be centered vertically with the textField itself
 
     It this flag is set the error button will be centered vertically with it's superview.
     
     This may be useful in some interfaces
    */
    
    @IBInspectable public var centerErrorButtonWithSuperview: Bool = false
    
    //TODO: Make another solution, more general than this one, that is specific to this project
    lazy var errorButton: UIButton? = {
        let errorButton = UIButton(frame: CGRect.zero)
        errorButton.setImage(self.errorButtonIconImage, for: UIControl.State.normal)
        
//        errorButton.frame.size = CGSize(
//            width: self.superview!.frame.size.height,
//            height: self.superview!.frame.size.height)
        
//        errorButton.frame.size = CGSize(width: 40.0, height: 40.0)
        
        errorButton.tintColor = self.errorButtonIconTintColor
        
        errorButton.contentMode = UIView.ContentMode.scaleAspectFit
        errorButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        errorButton.isHidden = false
        
        errorButton.contentEdgeInsets = UIEdgeInsets.zero
        errorButton.contentHorizontalAlignment = .fill
        errorButton.contentVerticalAlignment = .fill
        
        errorButton.addTarget(self, action: #selector(NeoformTextField.showErrorButtonMessage), for: .touchDown)
        
        //        errorButton.addTarget(self, action: #selector(FormTextField.showErrorButtonMessage), for: UIControlEvents.touchDown)
        
        /**
         Create the constraints.
         
         The button will ALWAYS be a subview of the button superview, but the alignment reference may change
        */
        
        errorButton.translatesAutoresizingMaskIntoConstraints = false
        
        errorButton.heightAnchor.constraint(equalToConstant: self.errorButtonIconSize.height).isActive = true
        errorButton.widthAnchor.constraint(equalToConstant: self.errorButtonIconSize.width).isActive = true
        
        let trailingConstraint = NSLayoutConstraint(
            item: errorButton,
            attribute: .trailing,
            relatedBy: .equal,
            toItem: self,
            attribute: .trailing,
            multiplier: 1,
            constant: 0)
        
        let centerVerticallyConstraint = NSLayoutConstraint(
            item: errorButton,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: self.centerErrorButtonWithSuperview ? self.superview! : self,
            attribute: .centerY,
            multiplier: 1,
            constant: 0)
        
        self.superview?.addSubview(errorButton)
        self.superview?.addConstraint(trailingConstraint)
        self.superview?.addConstraint(centerVerticallyConstraint)
        
        return errorButton
    }()
    
    /*
     Conformance to NeoformElement protocol
     
     UITextField already conforms to canBecomeFirstResponder and becomeFirstResponder
    */
    @IBInspectable public var name: String!
    public var collection: NeoformElementCollection?
    public var _nextElement: NeoformElement? {
        didSet {
            self.returnKeyType = .next
        }
    }
    
    
    /*
     The UITextField custom format
     
     The format can be nil, as long as the TextField has any kind of picker or autocomplete
    */
    public var format: NeoformTextFieldFormat? {
        didSet {
            if let format = self.format {
                self.keyboardType = format.keyboardType
                //The numberPad doesn't have a Return button, so create the accessory view
                switch self.keyboardType {
                case .numberPad:
                    self.createCloseInputAccessoryView()
                default:
                    break
                }
            }
        }
    }
    
    /*
     Helper property made only to allow us to set the field format directly from the storyboard
     
     We must pass a string that refers to a enum referenced on FormatsIndex
    */
    @IBInspectable public var fieldFormatIB: String? {
        didSet {
            if let formatIB = self.fieldFormatIB,
                let formatIndex = NeoformTextField.FormatsIndex(rawValue: formatIB) {
                self.format = formatIndex.format
            }
        }
    }
    
    /*
     Special text setter variable that allow us to set text after using the default format
     
     If no format is set we just set the text as it is
    */
    public var formattedText: String? {
        get {
            return self.text
        }
        set {
            if let notNilValue = newValue, notNilValue != "" {
                if let fieldFormat = self.format {
                    super.text = fieldFormat.format(notNilValue)
                }
                else if let inputView = self.inputView {
                    if inputView == self.datePicker {
                        self.dateFormatter.timeZone = TimeZone(identifier: "UTC")
                        self.dateFormatter.dateFormat = self.dateFormatSave ?? self.dateFormatShow!
                        if let date = self.dateFormatter.date(from: notNilValue) {
                            self.datePicker.date = date
                            self.datePickerChanged(self.datePicker)
                        }
                        //If no date don't show it on the field
                    }
                    else if inputView == self.pickerView {
                        //Search index of element
                        for component in 0..<self.pickerViewItems.count {
                            for row in 0..<self.pickerViewItems[component].count {
                                if self.pickerViewItems[component][row].name == notNilValue {
                                    //Found index for this value. Set it as selected!
                                    self.pickerView.selectRow(row, inComponent: component, animated: false)
                                    self.pickerView(self.pickerView, didSelectRow: row, inComponent: component)
                                    break
                                }
                            }
                        }
                    }
                    else {
                        super.text = newValue
                    }
                }
            }
            else {
                super.text = ""
            }
        }
    }
    
    /*
     The field has an own delegate by default but sometimes we need other delegate, so we can have other delegate on the field
    */
    internal var neoformDelegate: NeoformTextFieldDelegate!
    
    internal var customDelegate: UITextFieldDelegate?
    override public var delegate: UITextFieldDelegate? {
        get {
            return self.customDelegate
        }
        set {
            self.customDelegate = newValue
        }
    }
    
    //
    //Field Pickers
    //
    
    /*
     Create a inputAccessoryView with an OK button on the right above the picker/keyboard that acts like the return button
     
     UIDatePicker and UIPickerView call this method on their creation.
    */
    func createCloseInputAccessoryView() {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 400, height: 30))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(self.delegate?.textFieldShouldReturn(_:)))]
//        toolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), UIBarButtonItem(title: "OK", style: .done, target: self, action: #selector(NeoformTextField.textFieldShouldReturn(_:)))]
        toolbar.sizeToFit()
        self.inputAccessoryView = toolbar
    }
    
    /*
     DatePicker and related properties
 
     The datePicker is automatically created and enabled when a dateFormat, a minDate or a maxDate is set
     
     The dateFormatShow is used to show the date on the field when selected and MUST be set when a picker is used
     
     The dateFormatSave is used on validate(). If not set we default to the dateFormatShow
    */
    lazy var datePicker: UIDatePicker = {
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.locale = Locale.current
        datePicker.timeZone = TimeZone(identifier: "UTC")
        datePicker.addTarget(self, action: #selector(self.datePickerChanged(_:)), for: .valueChanged)
        return datePicker
    }()
    
    lazy var dateFormatter: DateFormatter = {
        let dateFmt = DateFormatter()
        dateFmt.locale = Locale.current
        dateFmt.timeZone = TimeZone(identifier: "UTC")
        return dateFmt
    }()
    
    public var datePickerMinimumDate: Date? {
        didSet {
            self.enableDatePicker()
            self.datePicker.minimumDate = self.datePickerMinimumDate
        }
    }
    
    public var datePickerMaximumDate: Date? {
        didSet {
            self.enableDatePicker()
            self.datePicker.maximumDate = self.datePickerMaximumDate
        }
    }
    
    public var dateFormatShow: String! {
        didSet {
            self.enableDatePicker()
        }
    }
    
    public var dateFormatSave: String? {
        didSet {
            self.enableDatePicker()
        }
    }
    
    @objc func datePickerChanged(_ datePicker: UIDatePicker) {
        self.dateFormatter.dateFormat = self.dateFormatShow!
        self.text = self.dateFormatter.string(from: datePicker.date)
    }
    
    func enableDatePicker() {
        self.inputView = self.datePicker
        self.createCloseInputAccessoryView()
    }
    
    /*
     PickerView and related properties
     
     The pickerView is automatically created when the pickerViewItems is set.
 
    */
    lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        return pickerView
    }()
    
    public var pickerViewItems = [[NeoformSelectableItemProtocol]]() {
        didSet {
            if self.pickerViewItems.count > 0 {
                self.enablePickerView()
            }
        }
    }
    
    public var pickerViewSelectedItem: NeoformSelectableItemProtocol?
    
    func enablePickerView() {
        self.inputView = self.pickerView
        
        self.createCloseInputAccessoryView()
    }
    
    /*
     This handler is called when creating the json and allows any conversion made to the selected item, in case you to cast the selected item to other class and use other field on the json
     
     If no handler is specified the ID is saved on the json by default
     */
    public var pickerSelectedItemToJson: ((NeoformSelectableItemProtocol) -> String)?
    
    /*
     This format shows how we should set the info from the pickerView components into the pickerView.
     If we have more than one component on the pickerView we should set this.
     All pickerComponentsValues are set as strings!
 
     */
    public var pickerViewComponentsFormat: String = "%@"
    
    /*
     This handler will be called when a item was selected on the pickerView
     
     It can be used to be notified of changes on the value of the textField,
     since the usual forms doesn't work with pickerView
 
     */
    
    public var onPickerViewSelectedItem: (() -> Void)? = nil
    
    /*
     PickerTableView and related properties
     
     The PickerTableView is a special picker that shows a table view with a search bar to allow the user to select an item
     
     It is recomended to be used on large lists
    */
    
    //A flag indicating where we have a pickerTableView or not
    public var hasPickerTableView: Bool = false
    
    //The viewController where to present the PickerTableView.
    public var viewToPresentPickerTableView: UIViewController?
    
    //Add a custom title to the pickerTableView. By default it's the field name.
    public var pickerTableViewTitle: String?
    
    //The handler used to load the picker table view items
    public var loadPickerTableViewItemsHandler: ((@escaping ([PickerTableViewElementProtocol]?) -> Void) -> Void)! {
        didSet {
            self.enablePickerTableView()
        }
    }
    
    public var pickerTableViewSelectedItem: PickerTableViewElementProtocol?
    
    public var onPickerTableViewSelectedItem: ((PickerTableViewElementProtocol) -> Void)?
    
    /*
     This handler is called when creating the json and allows any conversion made to the selected item, in case you to cast the selected item to other class and use other field on the json
     
     If no handler is specified the ID is saved on the json by default
     */
    public var pickerTableViewSelectedItemToJson: ((PickerTableViewElementProtocol) -> String)?
    
    func enablePickerTableView() {
        self.hasPickerTableView = true
        
        self.inputView = UIView(frame: CGRect.zero)
        self.addTarget(self, action: #selector(NeoformTextField.showPickerTableView), for: .editingDidBegin)
//        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(NeoformTextField.showPickerTableView)))
    }
    
    @objc func showPickerTableView() {
        let bundle = Bundle(for: NeoformTextField.self)
        
        let nav = UIStoryboard(name: "Main", bundle: bundle).instantiateViewController(withIdentifier: "pickerTableViewNavigationController") as! UINavigationController
        let pickerTableViewController = nav.viewControllers.first! as! PickerTableViewController
        
        pickerTableViewController.loadItemsHandler = self.loadPickerTableViewItemsHandler
        pickerTableViewController.selectedItemHandler = {
            selectedItem in
            self.pickerTableViewSelectedItem = selectedItem
            super.text = selectedItem.displayableName
            
            self.onPickerTableViewSelectedItem?(selectedItem)
            
            _ = self.delegate?.textFieldShouldReturn?(self)
        }
        pickerTableViewController.title = self.pickerTableViewTitle ?? self.name
        
        let viewControllerToPresent = self.viewToPresentPickerTableView ?? self.parentViewController!
        pickerTableViewController.presentingController = viewControllerToPresent
        viewControllerToPresent.present(nav, animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    /*
     Initializers
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.customInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }
    
    
    
    func customInit() {
        self.neoformDelegate = NeoformTextFieldDelegate(neoformTextField: self)
        super.delegate = self.neoformDelegate
        
        self.returnKeyType = .done
//        self.addObserver(self, forKeyPath: "selectedTextRange", options: [NSKeyValueObservingOptions.new, NSKeyValueObservingOptions.old], context: nil)
    }
    
//    deinit {
//        self.removeObserver(self, forKeyPath: "selectedTextRange")
//    }
    
    /*
     When we have a picker on the field there should be no caret shown on the field
    */
    override public func caretRect(for position: UITextPosition) -> CGRect {
        if let inputView = self.inputView {
            if inputView == self.datePicker || inputView == self.pickerView {
                return CGRect.zero
            }
        }
        
        if self.hasPickerTableView {
            return CGRect.zero
        }
        
        return super.caretRect(for: position)
    }
}

/*
 Conformance to NeoformElement protocol
 */

extension NeoformTextField {
    /*
     A field must have a format or a picker set.
     
     A format overrides any other picker that was previously set.
    */
    public func validate() throws -> [String:Any] {
        if let format = self.format {
            do {
                let value = try format.getValidValue(from: self.text!)
                if self.showErrorButton {
                    self.errorButton?.isHidden = true
                }
                return [self.name:value]
            }
            catch {
                let formError = error as! NeoformError
                try self.handleError(error: formError)
            }
            /*
            guard let value = format.getValidValue(from: self.text!) else {
                self.handleError()
                return nil
            }
            self.errorButton?.isHidden = true
            return [self.name:value]*/
        }
        else if let inputView = self.inputView, (inputView == self.datePicker || inputView == self.pickerView) {
            //If the text is empty that means the picker was not called, so it's invalid!
            if self.text == "" {
                try self.handleError(error: NeoformError.valueNotSelected(textField: self))
            }
            
            switch inputView {
            case self.datePicker:
                //Check if we have a special format to save or if it's the same
                let dateFormat: String = self.dateFormatSave ?? self.dateFormatShow!
                self.dateFormatter.dateFormat = dateFormat
                
                if self.showErrorButton {
                    self.errorButton?.isHidden = true
                }
                return [self.name:self.dateFormatter.string(from: self.datePicker.date)]
            case self.pickerView:
                //Check if we have a convert handler
                var value: String
                if let toJson = self.pickerSelectedItemToJson {
                    value = toJson(self.pickerViewSelectedItem!)
                }
                else {
                    value = self.pickerViewSelectedItem!.id
                }
                
                if self.showErrorButton {
                    self.errorButton?.isHidden = true
                }
                return [self.name:value]
            default:
                //The pickerTableView has a empty inputView
                //Is must never get here
                fatalError("The textField has an inputView but it is not a datePicker or a pickerView. This is not allowed.")
            }
        }
        else if self.hasPickerTableView {
            if self.text == "" {
                //Picker was not selected
                try self.handleError(error: NeoformError.valueNotSelected(textField: self))
            }
            //Since the text is not empty we have a selected value on the pickerTableView
            var value: String
            if let toJson = pickerTableViewSelectedItemToJson {
                value = toJson(self.pickerTableViewSelectedItem!)
            }
            else {
                value = self.pickerTableViewSelectedItem!.id
            }
            
            if self.showErrorButton {
                self.errorButton?.isHidden = true
            }
            return [self.name: value]
        }
        else {
            fatalError("NeoformTextField must have a format or a picker set!")
            
        }
        return [String:Any]()
    }
    
    public func handleError(error: NeoformError) throws {
        if self.showErrorButton {
            self.errorButton?.isHidden = false
//            if self.isEnabled {
//                self.becomeFirstResponder()
//            }
        }
//        else {
            //Default is to not show the error button
            switch error {
            case .empty(_):
                throw NeoformError.empty(element: self)
            case .invalidValue(_):
                throw NeoformError.invalidValue(textField: self)
            case .valueNotSelected(_):
                throw NeoformError.valueNotSelected(textField: self)
            default:
                throw NeoformError.unknown(element: self)
            }
//        }
    }
}

//Conformance to UITextFieldDelegate
extension NeoformTextField: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let format = self.format {
            return format.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        else {
            //TODO: Check for any picker if we don't have a format on the field

            //No format, so check for a customDelegate
            if let myDelegate = self.customDelegate {
                if let value = myDelegate.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) {
                    return value
                }
            }
        }

        return true
    }

    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if let myDelegate = self.customDelegate {
            myDelegate.textFieldDidBeginEditing?(textField)
        }
    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        if let myDelegate = self.customDelegate {
            myDelegate.textFieldDidEndEditing?(textField)
        }
    }

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.text == "", let inputView = self.inputView {
            switch inputView {
            case self.datePicker:
                self.datePickerChanged(self.datePicker)
            case self.pickerView:
                self.pickerView.selectRow(0, inComponent: 0, animated: false)
                self.pickerView(self.pickerView, didSelectRow: 0, inComponent: 0)
            default:
                break
            }
        }

        if let myDelegate = self.customDelegate {
            if let value = myDelegate.textFieldShouldBeginEditing?(textField) {
                return value
            }
        }

        return true
    }

    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let myDelegate = self.customDelegate {
            if let value = myDelegate.textFieldShouldEndEditing?(textField) {
                return value
            }
        }

        return true
    }

    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let myDelegate = self.customDelegate {
            if let value = myDelegate.textFieldShouldClear?(textField) {
                return value
            }
        }

        return true
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Resign the first responder
        self.resignFirstResponder()

        /*
         Look for a nextElement on this element or in any other parent collection.
        */
        if let nextElement = self.nextElement {
            nextElement.becomeFirstResponder()
        }

        if let myDelegate = self.customDelegate {
            if let value = myDelegate.textFieldShouldReturn?(textField) {
                return value
            }
        }

        return true
    }
}

//Conformance to UIPickerViewDataSource and Delegate
extension NeoformTextField: UIPickerViewDataSource, UIPickerViewDelegate {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return self.pickerViewItems.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerViewItems[component].count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerViewItems[component][row].name
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.pickerViewSelectedItem = self.pickerViewItems[component][row]
        
        var selectedComponentsTitle = [String]()
        for comp in 0..<pickerView.numberOfComponents {
            let selectedRow = pickerView.selectedRow(inComponent: comp)
            let selectedTitle = self.pickerView(pickerView, titleForRow: selectedRow, forComponent: comp)!
            selectedComponentsTitle.append(selectedTitle)
        }
        
        self.text = String(format: self.pickerViewComponentsFormat, arguments: selectedComponentsTitle)
        self.onPickerViewSelectedItem?()
    }
}

//show error message
extension NeoformTextField {
    @objc func showErrorButtonMessage() {
        //Get the view controller that holds the object
        if let parentViewController = self.parentViewController {
            let bundle = Bundle(for: NeoformTextField.self)
            
            let errorWarningViewController = UIStoryboard(name: "Main", bundle: bundle).instantiateViewController(withIdentifier: "errorWarningViewController") as! ErrorWarningViewController
            errorWarningViewController.errorMessage = self.errorMessage
            errorWarningViewController.errorColor = self.errorButtonIconTintColor
            errorWarningViewController.preferredContentSize = CGSize(width: 0, height: NSLocalizedString(self.errorMessage, comment: "").heightWithConstrainedWidth(width: 200, font: UIFont.systemFont(ofSize: 13.0)) + 30)
            errorWarningViewController.modalPresentationStyle = UIModalPresentationStyle.popover
            guard let popoverControllerDelegate = parentViewController as? UIPopoverPresentationControllerDelegate else {
                fatalError("The ViewController that contains the TextField must conforms to the UIPopoverPresentationControllerDelegate")
            }
            errorWarningViewController.popoverPresentationController?.delegate = popoverControllerDelegate
            errorWarningViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.up
            errorWarningViewController.popoverPresentationController?.backgroundColor = self.errorButtonIconTintColor
            errorWarningViewController.popoverPresentationController?.sourceView = self.errorButton!
            errorWarningViewController.popoverPresentationController?.sourceRect = self.errorButton!.bounds
            parentViewController.present(errorWarningViewController, animated: true, completion: nil)
        }
    }
}

//extension NeoformTextField {
//    public override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        if keyPath == "selectedTextRange" {
//            if let currentTextRange = self.selectedTextRange {
//                //Check if the textRange is on the last position
//                if currentTextRange.end != self.endOfDocument {
//                    //If not se must set it to the end
//                    let newRange = self.textRange(from: self.endOfDocument, to: self.endOfDocument)
//                    self.selectedTextRange = newRange
//                }
//            }
//        }
//    }
//}

//Get a view parentController
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
