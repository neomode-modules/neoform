//
//  NeoformTextFieldPasswordCollection.swift
//  Locksmith
//
//  Created by Edison Santiago on 06/09/17.
//

import Foundation

public class NeoformTextFieldPasswordCollection: NeoformElementCollection {
    private var passwordTextField: NeoformTextField
    private var confirmPasswordTextField: NeoformTextField
    
    public init(name: String, passwordTextField: NeoformTextField, confirmPasswordTextField: NeoformTextField) {
        self.passwordTextField = passwordTextField
        self.confirmPasswordTextField = confirmPasswordTextField
        self.passwordTextField.isSecureTextEntry = true
        self.confirmPasswordTextField.isSecureTextEntry = true
        
        super.init(name: name, elements: [passwordTextField, confirmPasswordTextField])
    }
    
    override public func validate() throws -> [String:Any] {
        var elementsDataDict = [String:Any]()
        /*
         Iterate through the elements and s each one of them.
         
         The collection is never responsible to handle the error of an element,
         it only gets notified of it and propagate the error up on the chain.
         */
        for element in self.elements {
            //If the element is invalid an error will be thrown
            let elementDataDict = try element.validate()
            elementsDataDict += elementDataDict
        }
        
        var values = [String]()
        //Get only the values from dictionary
        for (_, v) in elementsDataDict {
            values.append(v as! String)
        }
        
        //Check if both values are equal
        if values.first! != values.last! {
            throw NeoformError.passwordNotMatch(passwordCollection: self)
        }
        
        //Both values are equal! Return with the name of the collection
        return [self.name: values.first!]
    }
    
    public override var canBecomeFirstResponder: Bool {
        return true
    }
    
    public override func becomeFirstResponder() -> Bool {
        return self.confirmPasswordTextField.becomeFirstResponder()
    }
}
