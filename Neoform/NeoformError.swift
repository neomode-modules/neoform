//
//  NeoformError.swift
//  Neoform
//
//  Created by Edison Santiago on 10/07/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

public enum NeoformError: Error {
    case empty(element: NeoformElement?)
    
    case emptyCollection(collection: NeoformElementCollection)
    
    case invalidValue(textField: NeoformTextField?)
    
    case valueNotSelected(textField: NeoformTextField?)
    
    case noneSelected(checkCollection: NeoformCheckElementCollection?)
    
    case unknown(element: NeoformElement?)
    
    case passwordNotMatch(passwordCollection: NeoformTextFieldPasswordCollection?)
    
    
}
