//
//  NeoformCheckElementCollection.swift
//  Neoform
//
//  Created by Edison Santiago on 26/04/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import Foundation

public class NeoformCheckElementCollection: NeoformElementCollection {
    //NeoformElementCollection already conforms to the NeoformElement protocol

    
    /*
     If the selection is optional, that means we can have zero elements selected on the form at a given moment
 
     With this flag turned on it is impossible to unselect a check element by tapping on it if it's the last one
    */
    public var isSelectionOptional: Bool = false
    
    /*
     With this flag turned on it is impossible to select more than one element on the check list at the same time
     
     When one is selected the other are unselect automatically
    */
    public var isRadio: Bool = false
    
    /*
     Set if the validation should return the unselected values too
     
     By default the validation will only return the selected fields (the true ones)
     If this flag is true all fields will be returned
    */
    public var saveUnselectedValues: Bool = false
    
    /*
     Handler called when an element is selected
    */
    public var onElementSelected: ((NeoformCheckElement) -> Void)?
    
    /*
     The CheckElementCollection can never become a first responder because there is no input on it
    */
    override public var canBecomeFirstResponder: Bool {
        get {
            return false
        }
    }
    
    @discardableResult override public func becomeFirstResponder() -> Bool {
        return false
    }
    
    override public func validate() throws -> [String:Any] {
        if self.elements.count == 0 {
            /*
             An empty collection is not valid
             */
            throw NeoformError.emptyCollection(collection: self)
        }
        
        var elementsDataDict = [String:Any]()
        /*
         Iterate through the elements and s each one of them.
         
         The collection is never responsible to handle the error of an element,
         it only gets notified of it and propagate the error up on the chain.
         */
        for element in self.elements {
            //If the element is invalid an error will be thrown
            let elementDataDict = try element.validate()
            elementsDataDict += elementDataDict
        }
        
        /*
         Extra validation for form. We have to check the flags in order to notice if the field is really valid
        */
        
        var trueElements = elementsDataDict
        for (key, value) in trueElements {
            if let v = value as? Bool, v == true {
                continue
            }
            
            trueElements.removeValue(forKey: key)
        }
        //If the selection is not optional, the number of selected elements can't be 0
        if !self.isSelectionOptional, trueElements.count == 0 {
            throw NeoformError.noneSelected(checkCollection: self)
        }
        
        if self.saveUnselectedValues {
            //Return all elements
            return elementsDataDict
        }
        else {
            //Return only the elements with a true value
            return trueElements
        }
    }
    
    //handleError is the same as the superclass
}

extension NeoformCheckElementCollection {
    func canUnselectElement() -> Bool {
        //If the selection is optional we can unselect the element
        if self.isSelectionOptional {
            return true
        }
        // We can never unselect an element in a radio
        else if self.isRadio {
            return false
        }
        else {
            //We have to check if we have more than one element selected
            //If this is the only one selected we can not unselect it
            return self.elements.count > 1
        }
    }
    
    func willSelectElement(_ element: NeoformCheckElement) {
        //Check if we need to unselect all the other elements before selecting this one
        if self.isRadio {
            self.unselectAllElements()
        }
        
        self.onElementSelected?(element)
    }
    
    func unselectAllElements() {
        for element in self.elements {
            if let checkElement = element as? NeoformCheckElement {
                checkElement.setUnselected(force: true)
            }
        }
    }
}
