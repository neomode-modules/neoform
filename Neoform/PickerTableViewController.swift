//
//  PickerTableViewController.swift
//  Neoform
//
//  Created by Edison Santiago on 10/07/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import UIKit

class PickerTableViewController: UITableViewController, UISearchResultsUpdating, UISearchControllerDelegate {
    private var searchBar: UISearchBar!
    private var loadingView: UIView?
    
    var presentingController: UIViewController?

    var loadedItems: Bool = false
    var items = [PickerTableViewElementProtocol]()
    var filteredItems = [PickerTableViewElementProtocol]()
    
    var loadItemsHandler: ((@escaping ([PickerTableViewElementProtocol]?) -> Void) -> Void)!
    var selectedItemHandler: ((PickerTableViewElementProtocol) -> Void)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar = UISearchBar()
        self.searchBar.sizeToFit()
        self.searchBar.showsCancelButton = false
        self.searchBar.delegate = self
        self.navigationItem.titleView = self.searchBar
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 44
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        if let presentingVc = self.presentingController {
            self.configureUI(withViewController: presentingVc)
        }
        
        self.loadItems()
    }
    
    func loadItems() {
        self.createLoadingIndicator()
        
        self.loadItemsHandler(self.hasLoadedItems)
    }
    
    func hasLoadedItems(_ items: [PickerTableViewElementProtocol]?) {
        DispatchQueue.main.async {
            self.loadingView?.removeFromSuperview()
            self.searchBar.becomeFirstResponder()
            
            if let loadedItems = items {
                self.items = loadedItems
                self.tableView.reloadData()
            }
            else {
                //TODO: Handle error on request
            }
        }
    }

    func updateSearchResults(for searchController: UISearchController) {
        self.filteredItems.removeAll()
        self.filteredItems = self.items.filter({$0.displayableName.lowercased().contains(searchController.searchBar.text!.lowercased())})
        self.tableView.reloadData()
    }
    
    func createLoadingIndicator() {
        //Create loadingView
        let loadingView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.backgroundColor = self.view.backgroundColor
        
        //Create activity indicator
        let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = UIColor.lightGray
        activityIndicator.startAnimating()
        
        DispatchQueue.main.async {
            //Create view hierarchy
            loadingView.addSubview(activityIndicator)
            activityIndicator.centerXAnchor.constraint(equalTo: loadingView.centerXAnchor).isActive = true
            activityIndicator.centerYAnchor.constraint(equalTo: loadingView.centerYAnchor).isActive = true
            
            self.view.addSubview(loadingView)
            
            if #available(iOS 11.0, *) {
                loadingView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
                loadingView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
                loadingView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
                loadingView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
            } else {
                loadingView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
                loadingView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
                loadingView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
                loadingView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
            }
            
            self.view.bringSubviewToFront(loadingView)
        }
        
        self.loadingView = loadingView
    }
    
    func configureUI(withViewController presenterVC: UIViewController) {
        if let presenterNav = presenterVC.navigationController {
            self.navigationController?.navigationBar.barStyle = presenterNav.navigationBar.barStyle
            self.navigationController?.navigationBar.barTintColor = presenterNav.navigationBar.barTintColor
            self.navigationController?.navigationBar.isTranslucent = presenterNav.navigationBar.isTranslucent
            self.navigationController?.navigationBar.tintColor = presenterNav.navigationBar.tintColor
            self.navigationController?.navigationBar.shadowImage = presenterNav.navigationBar.shadowImage
            
            self.searchBar.searchBarStyle = .minimal
            self.searchBar.barStyle = presenterNav.navigationBar.barStyle
            self.searchBar.tintColor = presenterNav.navigationBar.tintColor
            self.searchBar.barTintColor = presenterNav.navigationBar.barTintColor
            
            
            self.searchBar.setImage(
                UIImage(named: "ic_searchbar_search", in: Bundle(for: PickerTableViewController.self), compatibleWith: nil),
                for: .search,
                state: .normal
            )
            self.searchBar.setImage(
                UIImage(named: "ic_searchbar_clear", in: Bundle(for: PickerTableViewController.self), compatibleWith: nil),
                for: .clear,
                state: .normal
            )

            //Set placeholder
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).attributedPlaceholder = NSAttributedString(string: "Buscar", attributes: [NSAttributedString.Key.foregroundColor: presenterNav.navigationBar.tintColor as Any])

            //Set textColor
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: presenterNav.navigationBar.tintColor as Any]
        }
    }

    @IBAction func dismissTapped(_ sender: Any) {
        self.searchBar.resignFirstResponder()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchBar.text!.count > 0 {//self.resultSearchController.isActive {
            return self.filteredItems.count
        }
        else {
            return self.items.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        
        if let dequeuedCell = tableView.dequeueReusableCell(withIdentifier: "searchItemCell") {
            cell = dequeuedCell
        }
        else {
            cell = UITableViewCell(style: .value1, reuseIdentifier: "searchItemCell")
        }
        
        let itemToDisplay = self.searchBar.text!.count > 0 ? self.filteredItems[indexPath.row] : self.items[indexPath.row]
        
        cell.textLabel?.text = itemToDisplay.displayableName
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = self.searchBar.text!.count > 0 ? self.filteredItems[indexPath.row] : self.items[indexPath.row]
        
        self.selectedItemHandler(selectedItem)
        
        self.searchBar.resignFirstResponder()
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}

extension PickerTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filteredItems.removeAll()
        if searchBar.text!.count > 0 {
            self.filteredItems = self.items.filter({$0.displayableName.lowercased().contains(searchBar.text!.lowercased())})
        }
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchBar.text!.count > 0 {
            searchBar.resignFirstResponder()
        }
    }
}

public protocol PickerTableViewElementProtocol {
    var id: String { get set }
    var displayableName: String { get } //This computed property is used to display the values on the table.
}
