//
//  NeoformElementCollection.swift
//  Neoform
//
//  Created by Edison Santiago on 01/04/17.
//  Copyright © 2017 Neomode Development Studio. All rights reserved.
//

import Foundation

/*
 A Collection of NeoformElements (a.k.a. the Form itself).
 
 -  The wrapper of all the form elements, and validator of them all
 
 -  The Collection can also be an element of a Neoform, so we can group
        similar data on specific forms.
        In a Profile form, for example, you can have separate forms for
        Personal Data, Address and Credit Card, all inside the same form.
 
 */
public typealias Neoform = NeoformElementCollection
public class NeoformElementCollection: NeoformElement {
    public var elements = [NeoformElement]()
    
    //Conformance to the NeoformElement protocol
    @IBInspectable public var name: String!
    public var collection: NeoformElementCollection?
    public var _nextElement: NeoformElement?
    @IBInspectable public var errorMessage: String = ""
    
    public var showErrorButton: Bool = false
    
//    public var onFormError: ((String) -> Void)?
    
    /*
     The collection can only "become" a first responder if it's first element can become one
     //(The collection itself never becomes the FirstResponder, only it's elements)
    */
    public var canBecomeFirstResponder: Bool {
        get {
            guard let firstElement = self.elements.first else {
                return false
            }
            return firstElement.canBecomeFirstResponder
        }
    }
    
    public func becomeFirstResponder() -> Bool {
        guard let firstElement = self.elements.first else {
            return false
        }
        
        return firstElement.becomeFirstResponder()
    }
    
    //Initialization
    public init(name: String, elements elementsArray: [NeoformElement]? = nil){
        self.name = name
        
        /*
         We use the append method to init the elements array to set the elements chain
         
         The append method sets the "nextElement" variable of the previous elements
         for each new element we add to it.
         */
        
        if let newElements = elementsArray {
            for newElement in newElements {
                self.append(newElement)
            }
        }
    }
    
    public func validate() throws -> [String:Any] {
        if self.elements.count == 0 {
            /*
             An empty collection is not valid
            */
            throw NeoformError.emptyCollection(collection: self)
        }
        
        var elementsDataDict = [String:Any]()
        /*
         Iterate through the elements and validate each one of them.
         
         The collection is never responsible to handle the error of an element,
         it only gets notified of it and propagate the error up on the chain.
        */
        for element in self.elements {
            //If an error occur on an element valiation it will be throwed to calling method
            let elementDataDict = try element.validate()
            elementsDataDict += elementDataDict
        }
        
//        return [self.name : elementsDataDict]
        return elementsDataDict
    }
    
    public func validateAll(onError: ((NeoformError) -> Void)) -> [String:Any] {
        var elementsDataDict = [String:Any]()
        /*
         Iterate through the elements and validate each one of them.
         
         The collection is never responsible to handle the error of an element,
         it only gets notified of it and propagate the error up on the chain.
         */
        for element in self.elements {
            //If an error occur on an element valiation the handler will be called
            do {
                let elementDataDict = try element.validate()
                elementsDataDict += elementDataDict
            }
            catch {
                onError(error as! NeoformError)
            }
        }
        
        //        return [self.name : elementsDataDict]
        return elementsDataDict
    }
    
    public func handleError(error: NeoformError) throws {
        throw error
    }
    /*
    public func handleError(errorMessage: String?) {
        if let collection = self.collection {
            collection.handleError(errorMessage: errorMessage)
        }
        else if let message = errorMessage {
            self.onFormError?(NSLocalizedString(message, comment: ""))
        }
    }*/
}

/*
 Functions related to the elements array
 */
extension NeoformElementCollection {
    /*
     Add a new element to the elements chain
     
     We set the reference to this collection on "collection" by default
     
     When we add a new element the last one got a reference to it automatically
     */
    
    public func append(_ newElement: NeoformElement){
        newElement.collection = self
        //Check if we already has an element on array
        if let previousElement = self.elements.last {
            //Set this new element as the nextElement of the previousElement
            previousElement.nextElement = newElement
        }
        self.elements.append(newElement)
    }
}

/*
 Add the merge operator to two dictionaries of the same type
 
 We need this in order to merge the dictionaries from the children elements on validate()
 */

public func += <KeyType, ValueType> (left: inout Dictionary<KeyType, ValueType>, right: Dictionary<KeyType, ValueType>) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

/*
 Static var to easy the access to the framework assets bundle
 */
extension NeoformElementCollection {
    static var assetsBundle: Bundle {
        get {
            return Bundle(for: NeoformElementCollection.self)
            /*let frameworkBundle = Bundle(for: NeoformElementCollection.self)
            let bundleUrl = frameworkBundle.url(forResource: frameworkBundle.infoDictionary!["CFBundleName"] as? String, withExtension: "bundle")!
            return Bundle(url: bundleUrl)!*/
        }
    }
}
