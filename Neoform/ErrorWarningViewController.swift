//
//  ErrorWarningViewController.swift
//  Neoform
//
//  Created by Edison Santiago on 11/04/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import UIKit

class ErrorWarningViewController: UIViewController {
    @IBOutlet weak var labelErrorMessage: UILabel!
    
    var errorColor: UIColor = UIColor.red
    var errorMessage: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = errorColor
        self.labelErrorMessage.text = NSLocalizedString(errorMessage, comment: "")
    }
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
//        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
}
