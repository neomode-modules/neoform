//
//  TablePickerView.swift
//  eSante
//
//  Created by Edison Santiago on 02/04/17.
//  Copyright © 2017 Neomode Development Studio. All rights reserved.
//

import UIKit

class TablePickerView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension TablePickerView: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var defaultCell: UITableViewCell
        if let cell = tableView.dequeueReusableCell(withIdentifier: "defaultCell") {
            defaultCell = cell
        }
        else {
            defaultCell = UITableViewCell(style: .default, reuseIdentifier: "defaultCell")
        }
        
        defaultCell.textLabel?.text = "Cell \(indexPath.row)"
        
        return defaultCell
    }
}
