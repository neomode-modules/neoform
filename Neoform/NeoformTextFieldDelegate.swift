//
//  NeoformTextFieldDelegate.swift
//  Neoform
//
//  Created by Edison Santiago on 08/08/2018.
//

import UIKit

class NeoformTextFieldDelegate: NSObject {
    var neoformTextField: NeoformTextField
    
    init(neoformTextField: NeoformTextField) {
        self.neoformTextField = neoformTextField
        
        super.init()
    }
}

extension NeoformTextFieldDelegate: UITextFieldDelegate {
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //For some reason sometimes the iOS autofill sends an space before the real value and if we "deny" it the autofill won't work
        if range.location == 0, range.length == 0, string == " " {
            return true
        }
        
        if let format = self.neoformTextField.format {
            var replacementString = string
            if replacementString.count > 1 {
                //We trim the replacement string because a space at the end is no reason to "deny" the change
                //(Yes, blame this on iOS AutoFill that sends the value with a space at the end)
                //If the string is only a space we don't remove it here (although the formatter might remove it)
                replacementString = replacementString.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            return format.textField(textField, shouldChangeCharactersIn: range, replacementString: replacementString)
        }
        else {
            //TODO: Check for any picker if we don't have a format on the field
            
            //No format, so check for a customDelegate
            if let myDelegate = self.neoformTextField.customDelegate {
                if let value = myDelegate.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) {
                    return value
                }
            }
        }
        
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if let myDelegate = self.neoformTextField.customDelegate {
            myDelegate.textFieldDidBeginEditing?(textField)
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        if let myDelegate = self.neoformTextField.customDelegate {
            myDelegate.textFieldDidEndEditing?(textField)
        }
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.neoformTextField.text == "", let inputView = self.neoformTextField.inputView {
            switch inputView {
            case self.neoformTextField.datePicker:
                self.neoformTextField.datePickerChanged(self.neoformTextField.datePicker)
            case self.neoformTextField.pickerView:
                self.neoformTextField.pickerView.selectRow(0, inComponent: 0, animated: false)
                self.neoformTextField.pickerView(self.neoformTextField.pickerView, didSelectRow: 0, inComponent: 0)
            default:
                break
            }
        }
        
        if let myDelegate = self.neoformTextField.customDelegate {
            if let value = myDelegate.textFieldShouldBeginEditing?(textField) {
                return value
            }
        }
        
        return true
    }
    
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if let myDelegate = self.neoformTextField.customDelegate {
            if let value = myDelegate.textFieldShouldEndEditing?(textField) {
                return value
            }
        }
        
        return true
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let myDelegate = self.neoformTextField.customDelegate {
            if let value = myDelegate.textFieldShouldClear?(textField) {
                return value
            }
        }
        
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //Resign the first responder
        self.neoformTextField.resignFirstResponder()
        
        /*
         Look for a nextElement on this element or in any other parent collection.
         */
        if let nextElement = self.neoformTextField.nextElement {
            nextElement.becomeFirstResponder()
        }
        
        if let myDelegate = self.neoformTextField.customDelegate {
            if let value = myDelegate.textFieldShouldReturn?(textField) {
                return value
            }
        }
        
        return true
    }
}
