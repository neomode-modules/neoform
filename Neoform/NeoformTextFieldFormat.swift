//
//  NeoformTextFieldFormat.swift
//  Neoform
//
//  Created by Edison Santiago on 01/04/17.
//  Copyright © 2017 Neomode Development Studio. All rights reserved.
//

import UIKit

/*
 Protocol to use on all NeoformTextFields
 */
public protocol NeoformTextFieldFormat {
    /*
     Minimum and maximum amount of chars allowed for this field.
     
     If minimum is zero this means this field is optional.
    */
    var minCharactersAllowed: Int { get set }
    var maxCharactersAllowed: Int { get set }
    
    /*
     The set of characters allowed to be typed for this field.
    */
    var allowedTypedCharactersSet: CharacterSet { get set }
    
    /*
     The set of characters allowed to be displayed (and pasted) for this field.
    */
    var allowedDisplayedCharactersSet: CharacterSet { get set }
    
    /*
     The keyboard type for the field
    */
    var keyboardType: UIKeyboardType { get set }
    
    /*
     Format any string given to the appropriate format for this mask.
     
     If the given string has invalid values this function should return nil.
    */
    func format(_ string: String) -> String?
    
    /*
     The mask of the field. Controls when the field value should change.
     
     To make the format easier this method should always returns false and the textField.text value should be changed inside this method when needed.
    */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    
    /*
     Get a valid string for this format from a given string
     
     We pass the string as a parameter so we don't need to reference the textField inside the format
    */
    func getValidValue(from: String) throws -> String
}

public extension NeoformTextFieldFormat {
    /*
     A default implementation for a simple format on the field
    */
    func format(_ string: String) -> String? {
        return string.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
    }
    
    /*
     A default implementation for a simple mask on the field
    */
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {        
        //If the string is only 1 character short this means we are typing it, so check for invalid TYPED characters
        if string.count == 1, string.rangeOfCharacter(from: self.allowedTypedCharactersSet.inverted) != nil {
            return false
        }
        //If the string is being pasted/quicktyped we need to check against DISPLAYED characters
        else if string.rangeOfCharacter(from: self.allowedDisplayedCharactersSet.inverted) != nil {
            return false
        }
        
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        //Check if the new string will exceed the maximum of characters allowed
        if newString.count > self.maxCharactersAllowed {
            return false
        }
        
        //Try to format the newString
        if let formattedString = self.format(newString) {
            textField.text = formattedString
            textField.sendActions(for: .editingChanged)
        }
        
        return false
    }
    
    
    
    
    /*
     A default implementation for a format validator

     We can't check for the character set here as a validation clause because there are many formats that add different characters to the value. A phone mask, for example, will have a decimalDigits set (because those are the only values that the user are allowed to enter) but the formatted String will probably have '(', ')', '-', and '+'.
     
     On the other hand, we use the characterset after validation to remove the characters outside the set. For example, we show the Phone Number pretty printed to a user, but this value will better be saved on our database as only numbers.
    */
    func getValidValue(from value: String) throws -> String {
        if self.minCharactersAllowed > 0 && value.count == 0 {
            throw NeoformError.empty(element: nil)
        }
        
        if !(value.count >= self.minCharactersAllowed && value.count <= self.maxCharactersAllowed) {
            throw NeoformError.invalidValue(textField: nil)
        }
        
        return value.components(separatedBy: self.allowedTypedCharactersSet.inverted).joined()
    }
}

/*
 Any element used on any of the NeoformTextField picker must conform to this protocol
*/
public protocol NeoformSelectableItemProtocol {
    var id: String { get set }
    var name: String { get set }
}

public struct NeoformSelectableItem: NeoformSelectableItemProtocol {
    public var id: String
    public var name: String
    
    public init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
