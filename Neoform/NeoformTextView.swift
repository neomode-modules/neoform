//
//  NeoformTextView.swift
//  Neoform
//
//  Created by Edison Santiago on 25/04/17.
//  Copyright © 2017 Neomode. All rights reserved.
//

import UIKit

public class NeoformTextView: UITextView, NeoformElement {
    /*
     Conformance to NeoformElement protocol
     
     UITextField already conforms to canBecomeFirstResponder and becomeFirstResponder
     */
    @IBInspectable public var name: String!
    public var collection: NeoformElementCollection?
    public var _nextElement: NeoformElement? {
        didSet {
            self.returnKeyType = .next
        }
    }
    @IBInspectable public var errorMessage: String = ""

    /*
     The TextView has no masks, the only text validation made to it is to check if the field can be empty or must have a value.
    */
    @IBInspectable var isOptional: Bool = true
    
    /*
     The TextView is it's own delegate by default and sometimes we need extra functions, so we can have other class set as a delegate on the TextView
     */
    internal var customDelegate: UITextViewDelegate?
    override public var delegate: UITextViewDelegate? {
        get {
            return self.customDelegate
        }
        set {
            self.customDelegate = newValue
        }
    }
    
    /*
     The UITextView default placeholder and related properties
     
     The layout structure for it is created on layoutSubviews if needed
    */
    internal var placeholderLabel: UILabel?
    
    @IBInspectable public var placeholderText: String?
    @IBInspectable public var placeholderTextColor: UIColor?
    
    /*
     Initializers
     */
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.customInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.customInit()
    }
    
    func customInit() {
        super.delegate = self
    }
    
    override public func awakeFromNib() {
        if let text = self.placeholderText {
            //Create placeholder
            self.placeholderLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 100, height: 20))
            self.placeholderLabel?.text = text
            self.placeholderLabel?.textColor = self.placeholderTextColor ?? UIColor.lightGray
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        if let label = self.placeholderLabel, label.superview != self {
            //Add as subview
            self.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            
            //Create placeholderLabel constraints
            self.addConstraints([
                NSLayoutConstraint(
                    item: label,
                    attribute: .leading,
                    relatedBy: .equal,
                    toItem: self,
                    attribute: .leading,
                    multiplier: 1,
                    constant: 8
                ),
                NSLayoutConstraint(
                    item: label,
                    attribute: .trailing,
                    relatedBy: .equal,
                    toItem: self,
                    attribute: .trailing,
                    multiplier: 1,
                    constant: 8
                ),
                NSLayoutConstraint(
                    item: label,
                    attribute: .top,
                    relatedBy: .equal,
                    toItem: self,
                    attribute: .top,
                    multiplier: 1,
                    constant: 8
                )
            ])
        }
    }
}

public extension NeoformTextView {
    func validate() throws -> [String:Any] {
        if !self.isOptional, super.text == nil, super.text.count == 0 {
            //Field is not optional but is empty...
            try self.handleError(error: NeoformError.empty(element: self))
        }
        return [self.name: super.text!]
    }
    
    func handleError(error: NeoformError) throws {
        throw error
    }
    /*
    func handleError(errorMessage: String? = nil) {
        //Propagates to the collection
        if let collection = self.collection {
            collection.handleError(errorMessage: errorMessage)
        }
    }*/
}


extension NeoformTextView: UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        self.placeholderLabel?.isHidden = true
        self.customDelegate?.textViewDidBeginEditing?(textView)
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if self.text.count == 0 {
            self.placeholderLabel?.isHidden = false
            self.customDelegate?.textViewDidEndEditing?(textView)
        }
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        self.customDelegate?.textViewDidChange?(textView)
    }
    
    public func textViewDidChangeSelection(_ textView: UITextView) {
        self.customDelegate?.textViewDidChangeSelection?(textView)
    }
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return self.customDelegate?.textViewShouldBeginEditing?(textView) ?? true
    }
    
    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return self.customDelegate?.textViewShouldEndEditing?(textView) ?? true
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return self.customDelegate?.textView?(textView, shouldChangeTextIn: range, replacementText: text) ?? true
    }
}
