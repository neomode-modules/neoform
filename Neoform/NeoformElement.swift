//
//  NeoformElement.swift
//  Neoform
//
//  Created by Edison Santiago on 01/04/17.
//  Copyright © 2017 Neomode Development Studio. All rights reserved.
//

import UIKit

public protocol NeoformElement: class {
    /*
     The name of the element is the value we set for it on the dictionary returned on valiation.
    */
    var name: String! { get set }
    
    /*
     The element collection that this element belongs to.
     
     If the Collection is nil this is the root object.
    */
    var collection: NeoformElementCollection? { get set }
    
    /*
     The next element on the collection this elements belongs to.
     
     If the nextElement is nil this is the last object of the form.
    */
    var _nextElement: NeoformElement? { get set }
    
    /*
     Property nextElement that returns the next element from this element or from it's collection
 
     If this returns nil this is the last object of the entire form.
    */
    var nextElement: NeoformElement? { get set }
    
    /*
     Property that holds the errorMessage for the field.
     
     The message can be the code for the localized one or the message itself.
    */
    var errorMessage: String { get set }
    
    /*
     Validate the form and get the data dictionary for it.
     
     Returns nil on any failure in any nested element or form.
    */
    func validate() throws -> [String:Any]
    
    /*
     Called inside validate() and handles the error
    */
    func handleError(error: NeoformError) throws
    
    /*
     Check if this element can "become" a FirstResponder.
     
     Returns true when this element or it's first child can become a FirstResponder.
     
     UIKit elements (UITextField, UITextView) already implement this method by default.
    */
    var canBecomeFirstResponder: Bool { get }
    
    /*
     Make this element becomes the first responder.
     
     UIKit elements (UITextField, UITextView) already implement this method by default.
    */
    @discardableResult func becomeFirstResponder() -> Bool
}

extension NeoformElement {
    public var nextElement: NeoformElement? {
        get {
            //Look for the nextElement on the element itself
            if let next = self._nextElement {
                return next
            }
            //If we have a formCollection, return the nextElement from it
            if let collection = self.collection {
                return collection.nextElement
            }
            //No nextElement
            return nil
        }
        set {
            self._nextElement = newValue
        }
    }
}
